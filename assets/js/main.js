let currentSearchTerm = '';

onload = function () {
    var e = document.getElementById('search-input');
    e.oninput = changeHandler;
    e.onpropertychange = e.oninput; // for IE8
    document.getElementById('search-input').addEventListener('keyup', handleKeyPress);

    $('#post-commit-form').form({
        fields: {
            name    : 'empty',
            email   : 'email',
            comment : 'empty'
        }
    });
};

function changeHandler(e) {
    currentSearchTerm = e.target.value;
}

function handleKeyPress(e) {
    if (e && e.key === 'Enter') {
        handleSubmit();
    }
}

function handleSubmit() {
    window.open('https://www.google.com/search?q=site:' + window.location.hostname + ' ' + currentSearchTerm);
}

function submitComment(form, api) {

    function onSuccess() {
        form.removeClass("loading");
        form.removeClass("error");
        form.addClass("success");
        form.form("clear");
    }
    
    function onFail(error) {
        form.removeClass("loading");
        form.removeClass("success");
        form.addClass("error");
        form.find("#error-code").text();
    }

    if (form.form("is valid")) {
        var values = form.form("get values");
        form.addClass("loading");
        $.post(
            api, values
        ).done(onSuccess).fail(onFail);
    }
}
