---
layout: post
title: Setting console font size on HiDPI screens in Fedora
date: 2017-01-21 00:00 +0000
---

I recently switched to another laptop which has a HiDPI screen. As usual, the thing is running Fedora (currently in version 25). Especially the KDE desktop was quite easy to configure to be usable; basically, it is a thing of starting **System Settings**, going to **Display and Monitor** and using the **Scale Display** button to bring up a neat configuration dialog which allows you to set a scaling factor for the monitor. For my particular setup, a factor of 2 works quite nice for the UI to be usable. 

[![]({{ "images/2017-01-21-setting-console-font-size-on-hidpi-screens-in-fedora/Screenshot_20170121_183819-preview.png" | relative_url }})]({{ "images/2017-01-21-setting-console-font-size-on-hidpi-screens-in-fedora/Screenshot_20170121_183819.png" | relative_url }})

One open point was the font size in the Linux console as well as in GRUB2. However, some configuration changes later this was fixed as well ;-)

## Increasing Font Size of GRUB2

First, I generated a custom font file from one of the TTF fonts installed on my system (in this case from Google Noto):

```bash
sudo mkdir /boot/grub2/fonts
sudo grub2-mkfont -s 36 -o /boot/grub2/fonts/NotoSansRegular36.pf2 \
 /usr/share/fonts/google-noto/NotoSans-Regular.ttf
```

This font can now be used in `/etc/default/grub`:

```bash
GRUB_TIMEOUT=5 
GRUB_DISTRIBUTOR="$(sed 's, release .*$,,g' /etc/system-release)" 
GRUB_DEFAULT=saved 
GRUB_DISABLE_SUBMENU=true 
GRUB_TERMINAL_OUTPUT="gfxterm"** 
GRUB_CMDLINE_LINUX="..." 
GRUB_DISABLE_RECOVERY="true" 
GRUB_FONT="/boot/grub2/fonts/NotoSansRegular36.pf2"
```

Basically, I added the `GRUB_FONT` entry which points to the font to use. In addition, I had to change the `GRUB_TERMINAL_OUTPUT` from `console` to `gfxterm`.

**Note:** The remaining entries must remain as they are! Only edit the two variables. Finally, the GRUB configuration can be regenerated. Depending on whether you have EFI or a BIOS, this can be done via:

```bash
# BIOS
sudo grub2-mkconfig -o /boot/grub2/grub.cfg

# EFI:
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```


## Increasing the Console Font Size

To increase the font size for the Linux console (`Ctrl+Alt+F[2-8]`), first install the `terminus` fonts:


```bash
sudo dnf install terminus-fonts-console
```

Now, change to a TTY and use the `setfont` command to load one of the fonts. You will find them in `/usr/lib/kbd/consolefonts/`. For me, the `ter-m32n` font works quite nicely:

```bash
sudo setfont ter-m32n
```

To make this font the default console font, edit `/etc/vconsole.conf` and adjust the `FONT` entry:

```bash
KEYMAP="de" 
FONT="ter-m32n"
```
