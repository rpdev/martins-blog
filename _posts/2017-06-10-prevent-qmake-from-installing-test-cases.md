---
layout: post
title: Prevent qmake from installing test cases
date: 2017-06-10 00:00 +0000
---

If you use `qmake` to build your Qt based projects and at the same time the [QtTest](http://doc.qt.io/qt-5/qttest-index.html) framework to write unit tests, you might have run into a strange behavior:

Generated Makefiles have an `installs` feature, which mainly is used on Unix like operating systems to install a previously compiled project. Normally you would expect the installs feature only puts your libraries and executable files into the target location. However, unit tests also get installed by default - which, usually, is not what's wanted, right?

Fortunately, it's quite easy to tell `qmake` not to install unit tests by default, simply add the `no_testcase_installs` configuration switch to your unit test's `.pro` file:

```qmake
TARGET   = my_unit_test
CONFIG  += console testcase no_testcase_installs
QT      += testlib
# ...
```

That should already do the trick ;-)
