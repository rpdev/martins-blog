---
layout: post
title: Intelligent Playlists - Part 1
date: 2009-12-03 00:00 +0000
---

So, beneath the usual university stuff and my job I have to do a practical work this term for the _Applied Systems of Artificial Intelligence_ course I'm attending currently. Beneath, the complete area of AI is very interesting and if you have a bit time consider doing some research there: AI is not only connected to robots and automated vacuum cleaners (although these might be most impressive examples). Back to topic: As I'm currently a bit tired of math (yeah, the last days where full of it) I decided to write down some of my thoughts about my task (usually, this helps in understanding and might be useful for others). So, here is it:

## Intelligent Playlists

Currently, media (and more specially music) players provide two ways to populate a playlist: You can manually add the songs you want to hear, or automate the process. Understandably, I'm currently not interested to improve the former one (how to better populate a playlist by hand is rather a task for those who understand important points of (good) user interfaces - and I surely do not belong to them). More interesting is the possibility to let the application create a playlist for you. But... how is this done? Currently (at last what I've seen so far) most music players support _dynamic playlists_ (which, however, are sometimes too called intelligent playlists). These dynamic playlists allow you to specify some criteria (as interpret, genre, rating, etc pp) and then fills the playlist accordingly. Well, this is cool so far. Well, now the AI course came in: One topic we discussed there was _similarity search_. The idea is: You search in a given data base (which is called case base here) for other cases that are (maybe more or less) similar to a given one. What would allow this in context of playlists? Now, imagine you have dropped already some files to the playlist. Wouldn't it be cool to just turn on the auto pilot and let the player populate the playlist with _similar tracks?_ The remaining problem: How to determine similarity?

### The lazy way

One way (I admittedly didn't though of til 5min ago): Just analyze the tracks in the list and create a dynamic playlist that (to some degree) will bring up similar songs. Indeed, this could be successful, as indeed tracks with similar tagging will be similar.

### Make a similarity search using tags

Another way would be to manually make a search. As above, this would probably result in good results.

### Analyze the tracks

The above methods both work by comparing the tags of two songs. However, this assumes that

*   The songs are tagged correctly (which is not necessarily true)
*   Tags reflect how a song sounds like

Both points can cause trouble for the first two methods, as they rely on both points to be true. Now, actually it _is_ possible to compare two songs by how they really sound: It would be possible to analyze the actual sound data and use this to determine similarity. Nevertheless, I probably will use the comparison of tags as it is faster, but keep the last way in mind.

### What about user interaction

Currently, I don't think too much user interaction would be good. The sense of any dynamically populated playlist is to create an exiting experience with no to low user interaction. Nevertheless, allowing some user interaction would make it possible to use just another feature: Learning algorithms. When determining similarity between two tracks, they are compared tag by tag (e.g. by a string comparison). Then these values are summed up and normalized. However... are all tags equally important? Not necessarily! So, by letting the user the chance to give _feedback_ to the algorithm, it is possible to improve the coefficients used which would result in generally better results. OK, so far for now ^^