---
layout: post
title: Fedora 21 (or later) not resuming from Suspend to Disk
date: 2015-05-03 00:00 +0000
---

Consider this as a memo to myself (or a useful hint in case you ran into a similar issue recently ;) ): Some days ago, my Asus Zenbook stopped to properly resume from Suspend to Disk state. I was running Fedora 21 (upgraded to 22 beta during the problem in the hope it was a caused by some bad Kernel update). As this did not help as well, I had to dig further and in fact found the issue: It seems, that some recent update or some stupid configuration issue on my side (or an unlucky combination of both xD ) caused the Grub2 configuration to omit the `resume` kernel command line option. This option tells the kernel where the disk image is stored which conserves the system state. To fix the issue, first, find out which is your swap partition, e.g. using `blkid`:

```
martin@zenbook:~$ blkid | grep swap
/dev/mapper/vg_zenbook-lv_swap: UUID="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" TYPE="swap"
```


So in my case, the swap device is `/dev/mapper/vg_zenbook-lv_swap`. With that information, you can now edit `/etc/default/grub` and append the resume argument to the kernel command line:

```bash
GRUB_CMDLINE_LINUX="[...] resume=/dev/mapper/vg_zenbook-lv_swap"
```

**Note:** I left out the existing parameters from my configuration -- whatever you do, do not remove anything but just append the `resume=/path/to/your/swap/device` argument. Finally, you can regenerate the grub configuration. In my case (as I am using EFI), I had to run

```bash
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

That's it. At least in my case, Suspend to Disk is working properly again after this change :)
