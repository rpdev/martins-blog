---
layout: post
title: Mac OS like gestures on Linux
date: 2014-08-03 00:00 +0000
---

Recently, I changed from a Windows based PC on my work to a MacBook Air. That's really great, mostly because of Mac OS provides a lot of the features I love on Linux and that make working especially with a lot of open application windows quite easy. Now, Mac OS also comes with excellent gesture support. I never really "missed" that feature (this is the first time I came in contact with Mac OS), but quickly came accustomed to that, too. Being able to show all open windows or get an overview of your virtual desktops with just a swipe on your trackpad is great. So the next weekend project was clear: Getting something like that for Linux, too ;) In fact, Canonical has implemented such gesture handling for Ubuntu already. Unfortunately, the required packages (namely, their `utouch` framework) is not easily available for Fedora, which I am using. So, some more manual work was required. Good news: Actually it is quite straightforward to install everything as long as you know what you need ;) So, here we go:


## Preparing your environment

By default, the installation procedure for source builds will put the utouch framework into `/usr/local`. That is okay (so these files won't interfere with anything typically installed via the package manager of your distribution). However, at least in my case I needed to setup some environment variables so the subsequent build commands would work and applications would find libraries at runtime. Without further ado:

```bash
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH
export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64/$LD_LIBRARY_PATH
```

Execute this once in a terminal (and keep it open for the subsequent builds). If you plan to install utouch to a different location, adjust the paths accordingly.

## Build Tools

You will of course also need the typical development tools in order to get everything done. As I had a lot of stuff already installed, I cannot tell which exact packages you need, however, typically the errors produced by the configure scripts will be informative enough for you to know. In addition, you have to install Qt4 and the accompanying development package (we will need it for the actual gesture recognition application later on).

## Install utouch

First of all, we have to install the utouch framework. You can find it on Launchpad. We needs to install four components: utouch-evemu, utouch-frame, utouch-grail and utouch-geis. You can either get the sources via bazar or just download the latest release as a zipped tarball. I decided for the latter. Below, find the links to Launchpad, together with the tarballs I used. Just fetch and install the packages in the order given below:


| **Package** | **Link to Launchpad Project** | **Tarball used (check on website for newer versions)** |
| ---- | ---- | --- |
| utouch-evemu | [https://launchpad.net/evemu](https://launchpad.net/evemu) | https://launchpad.net/evemu/trunk/evemu-1.0.10/+download/evemu-1.0.10.tar.gz |
| utouch-frame | [https://launchpad.net/frame](https://launchpad.net/frame) | https://launchpad.net/frame/trunk/v2.5.0/+download/frame-2.5.0.tar.gz |
| utouch-grail | [https://launchpad.net/grail](https://launchpad.net/grail) | https://launchpad.net/grail/trunk/3.1.0/+download/grail-3.1.0.tar.gz |
| utouch-geis | [https://launchpad.net/geis](https://launchpad.net/geis) | https://launchpad.net/geis/trunk/2.2.16/+download/geis-2.2.16.tar.gz |

For each of these packages:

1.  Download it either by getting the latest release tarball or download via bazar.
2.  Change into the directory and execute the usual build and installation steps:

```bash
./configure && make && sudo make install
```

## Installation of touchegg and touchegg-gce

Now that we have utouch available, the next step is to install an appropriate application that will recognize the actual gestures and trigger appropriate actions. For that, I installed touchegg and (for graphical configuration) touchegg-gce. Both are Qt based applications, hence make sure you have Qt4 and the Qt development packages installed. First, you want to install touchegg. You can find it as a project on [Launchpad]("https://launchpad.net/touchegg). When I checked, there were no release tarballs, so I used

```bash
bzr branch lp:touchegg
```

to get the code via bazar. Change into the directory and issue

```
qmake && make && sudo make install
```

to build and install touchegg. Note, that this will install it into `/usr` (instead of `/usr/local`). Now, you can just execute `touchegg` to run it. This will create a configuration file in `$HOME/.config/touchegg` (if it not already exists). You can edit this file to change the gestures recognized and their associated actions. If you prefer a GUI for editing this file, you can use [Touchegg-gce](https://github.com/Raffarti/Touchegg-gce). This application allows you to load, modify and save the touchegg configuration. As it is hosted on GitHub, use git to get it before building:

```bash
git clone https://github.com/Raffarti/Touchegg-gce.git
cd Touchegg-gce
qmake && make
```

Note that Touchegg-gce does not come with an installation procedure. Instead, just start it from where you also build it.

## Some last steps...

Finally, you might want to do some configuration depending on your system and Desktop Environment. First of all, touchegg might interfere with the synaptics input driver for some gestures. To circumvent this, create a script `$HOME/bin/prepare-touchpad-for-touchegg.sh`. In this script we'll use synclient to setup the synaptics driver appropriately. In my case, I want synaptics to handle one and two finger events and Touchegg 3 and 4 finger ones. For this to work, one has to disable 3 finger gestures in synaptics:

```bash
#!/bin/bash

# If you want Touchegg to handle 2 finger gestures, deactivate
# 2 finger gestures in synaptics:
#synclient TapButton2=0

# Same for 3 finger gestures:
synclient TapButton3=0

# Same for 2 finger clicks:
#synclient ClickFinger2=0

# And for 3 finger clicks:
synclient ClickFinger3=0

# If Touchegg shall take care for scrolling, 
# deactivate it in synaptics:
#synclient HorizTwoFingerScroll=0
#synclient VertTwoFingerScroll=0
```

Make it executable:

```bash
chmod +x $HOME/bin/prepare-touchpad-for-toughegg.sh
```

And ensure it is started when your desktop starts. For example, when using KDE, fire up `systemsettings` and go to _Startup and Shutdown_ and add your script via _Add Program_. Next, you want to ensure that touchegg itself is started on desktop startup. For this, create a second script `$HOME/bin/touchegg.sh` with the following content:

```bash
#!/bin/bash
export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64/$LD_LIBRARY_PATH
touchegg &
```

Make this one executable, too:

```bash
chmod +x $HOME/bin/touchegg.sh
```

And also add it to the startup procedure of your desktop environment. That script does nothing else than starting touchegg; however, as at least in my case the utouch libraries were not automatically found, I had to modify `LD_LIBRARY_PATH` to point to `/usr/local` . Last but not least, in order to have Touchegg-gce easily available, I created a similar script for it as well:

```bash
#!/bin/bash
export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64/$LD_LIBRARY_PATH
/path/to/source/of/Touchegg-gce/touchegg-gce
```

Note that you want to adjust the path to point to the correct location where you stored Touchegg-gce ;) That's it!
