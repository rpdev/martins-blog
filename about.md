---
layout: page
title: About
permalink: /about/
---

This is the personal blog of Martin Höher. I am sharing here some thoughts and experiences usually regarding my professional and private programming stuff - in the hope that it might be useful for someone (and at least serve me as a kind of backup as well).