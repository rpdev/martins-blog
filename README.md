# Blog of Martin Hoeher (@mhoeher) based on Wall-E

Wall-E is a modern Jekyll template with a magazine-like grid layout on the frontpage, beautiful typography and uncomplicated content. Visit its [home page](https://github.com/abhn/Wall-E) to learn more.
